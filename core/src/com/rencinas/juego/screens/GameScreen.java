package com.rencinas.juego.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.rencinas.juego.MyGdxGame;
import com.rencinas.juego.character.Soldier;

public class GameScreen implements Screen {

    final MyGdxGame game;
    SpriteBatch batch;
    Soldier soldier;

    public GameScreen(MyGdxGame game) {
        this.game = game;
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float dt) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        soldier.render(batch);
        batch.end();
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }
}
