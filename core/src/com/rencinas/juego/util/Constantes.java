package com.rencinas.juego.util;

/**
 * Created by Raúl on 08/02/2016.
 */
public class Constantes {
    public static final int SCREEN_WIDTH = 1024;
    public static final int SCREEN_HEIGHT = 512;

    public static final int PLAYER_WIDTH = 16;
    public static final int PLAYER_HEIGHT = 32;

    public static final int TILE_WIDTH = 16;
    public static final int TILE_HEIGHT = 16;

    public static final int LIVES = 3;
}
