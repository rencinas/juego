package com.rencinas.juego.character;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.rencinas.juego.util.Constantes;

/**
 * Created by Raúl on 08/02/2016.
 */
public class Soldier {
    public static final float SPEED = 150f;

    public Vector2 position;

    Animation rightAnimation;
    Animation leftAnimation;
    Animation upAnimation;
    Animation idleAnimation;
    TextureRegion currentFrame;
    float stateTime;
    int lives;

    public Vector2 velocity = new Vector2();
    public enum State {
        RIGHT, LEFT, UP, IDLE
    }
    public State state;

    public Soldier(float x, float y, int lives) {

        position = new Vector2(x, y);
        this.lives = lives;
        state = State.IDLE;
    }

    public void move(Vector2 movement) {

        movement.scl(SPEED);
        position.add(movement);
    }

    public void render(SpriteBatch batch) {

        batch.draw(currentFrame, position.x, position.y,Constantes.PLAYER_WIDTH, Constantes.PLAYER_HEIGHT);
    }

    public void update(float dt) {

        stateTime += dt;
        switch (state) {
            case RIGHT:
                currentFrame = rightAnimation.getKeyFrame(stateTime, true);
                break;
            case LEFT:
                currentFrame = leftAnimation.getKeyFrame(stateTime, true);
                break;
            case UP:
                currentFrame = upAnimation.getKeyFrame(stateTime, true);
                break;
            case IDLE:
                currentFrame = idleAnimation.getKeyFrame(stateTime, true);
                break;
        }

        if (position.x <= 0)
            position.x = 0;
        System.out.println(position.x +" "+ currentFrame);
        if ((position.x + currentFrame.getRegionWidth()) >= Constantes.SCREEN_WIDTH)
            position.x = Constantes.SCREEN_WIDTH - currentFrame.getRegionWidth();
    }
}
