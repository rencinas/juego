package com.rencinas.juego.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import java.util.HashMap;
import java.util.Map;


public class ResourceManager {

	private static Map<String, TextureAtlas> atlas = new HashMap<String, TextureAtlas>();
	private static Map<String, Animation> animations = new HashMap<String, Animation>();
	
	public static void loadAllResources() {


        loadResource("soldier", new TextureAtlas(Gdx.files.internal("core/assets/img/soldier.pack")));

        loadResource("soldier_rigth", new Animation(0.15f, atlas.get("soldier").findRegions("soldier_right")));
        loadResource("soldier_left", new Animation(0.15f, atlas.get("soldier").findRegions("soldier_left")));
        loadResource("soldier_up", new Animation(0.15f, atlas.get("soldier").findRegions("soldier_up")));
        loadResource("soldier_down", new Animation(0.15f, atlas.get("soldier").findRegions("soldier_down")));
	}
	
	private static void loadResource(String name, TextureAtlas textureAtlas) {
		atlas.put(name, textureAtlas);
	}
	
	private static void loadResource(String name, Animation animation) {
		animations.put(name, animation);
	}
	
	public static TextureAtlas getAtlas(String name) {
		return atlas.get(name);
	}
	
	public static Animation getAnimation(String name) {
		return animations.get(name);
	}
}
