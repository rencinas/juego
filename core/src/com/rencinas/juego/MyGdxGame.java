package com.rencinas.juego;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.rencinas.juego.character.Soldier;

public class MyGdxGame extends ApplicationAdapter {
	BitmapFont fuente;
    SpriteBatch spriteBatch;
    Soldier soldier;

	@Override
	public void create () {
		spriteBatch = new SpriteBatch();

        soldier = new Soldier(10,10,5);

	}

	@Override
	public void render () {
        float dt = Gdx.graphics.getDeltaTime();
        
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        handleInput(dt);
        update(dt);
	}

    private void handleInput(float dt) {

        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            soldier.state = Soldier.State.LEFT;
            soldier.move(new Vector2(-dt, 0));
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            soldier.state = Soldier.State.RIGHT;
            soldier.move(new Vector2(dt, 0));
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.W)) {
            soldier.state = Soldier.State.UP;
            soldier.move(new Vector2(0, dt));
        }
        else
            soldier.state = Soldier.State.IDLE;
    }

    private void update (float dt){
        soldier.update(dt);
    }

    @Override
    public void dispose() {
        spriteBatch.dispose();
        fuente.dispose();
    }
}
