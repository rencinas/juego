package com.rencinas.juego.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.rencinas.juego.MyGdxGame;
import com.rencinas.juego.util.Constantes;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "NullPointerException";
        config.width = Constantes.SCREEN_WIDTH;
        config.height = Constantes.SCREEN_HEIGHT;
		new LwjglApplication(new MyGdxGame(), config);
	}
}
